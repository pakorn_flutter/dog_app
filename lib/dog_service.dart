import 'dog.dart';

var lastId = 3;
var mockDogs = [Dog(id: 1, name: 'A', age: 8), Dog(id: 2, name: 'B', age: 5)];

int getNewid() {
  return lastId++;
}

Future<void> addNewDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    mockDogs.add(Dog(id: getNewid(), name: dog.name, age: dog.age));
  });
}

Future<void> saveDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDogs.indexWhere((item) => item.id == dog.id);
    mockDogs[index] = dog;
  });
}

Future<void> delDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDogs.indexWhere((item) => item.id == dog.id);
    mockDogs.removeAt(index);
  });
}

Future<List<Dog>> getDogs() {
  return Future.delayed(Duration(seconds: 1), () => mockDogs);
}
