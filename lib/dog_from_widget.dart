import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'dog.dart';
import 'dog_service.dart';

class DogForm extends StatefulWidget {
  Dog dog;
  DogForm({Key? key, required this.dog}) : super(key: key);

  @override
  _DogFormState createState() => _DogFormState(dog);
}

class _DogFormState extends State<DogForm> {
  final _formKey = GlobalKey<FormState>();
  Dog dog;
  _DogFormState(this.dog);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Dog Form')),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: [
              TextFormField(
                autofocus: true,
                initialValue: dog.name,
                decoration: InputDecoration(labelText: 'Name'),
                onChanged: (String? value) {
                  dog.name = value!;
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input name ';
                  }
                  return null;
                },
              ),
              TextFormField(
                initialValue: dog.age.toString(),
                decoration: InputDecoration(labelText: 'Age'),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                ],
                onChanged: (String? value) {
                  dog.age = int.parse(value!);
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input age ';
                  }
                  return null;
                },
              ),
              ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (dog.id > 0) {
                        await saveDog(dog);
                      } else {
                        await addNewDog(dog);
                      }
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save'))
            ],
          ),
        ),
      ),
    );
  }
}
